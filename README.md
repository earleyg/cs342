# The Real App Store (FunList)

An app by Graham Earley and Simon Orlovsky for Carleton College CS342.

Among the things we experimented with:

* Custom-defined animations between activities.
* All sorts of Views (Web, Picture, Text, etc).
* Dependencies / external libraries (for calendar reading functionality)
* MediaPlayer (for strikeouts!)