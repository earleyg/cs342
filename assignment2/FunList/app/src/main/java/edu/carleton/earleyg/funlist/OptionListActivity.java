package edu.carleton.earleyg.funlist;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class OptionListActivity extends ListActivity {
    private static final int PICTURE_ACTIVITY_POSITION = 0;
    private static final int COUNTER_ACTIVITY_POSITION = 1;
    private static final int ACTIVITY_2 = 2;
    private static final int ACTIVITY_3 = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_option_list);

        ArrayList<String> options = new ArrayList<String>();
        options.add(getResources().getString(R.string.title_activity_calendar));
        options.add(getResources().getString(R.string.title_activity_counter));
        options.add(getResources().getString(R.string.title_activity_web));
        options.add(getResources().getString(R.string.title_activity_ninja));

        OptionListAdapter adapter = new OptionListAdapter(this, R.layout.list_cell, options);
        ListView lv = (ListView)this.findViewById(android.R.id.list);
        lv.setAdapter(adapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_option_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick(ListView parent, View v, int position, long id) {
        final Intent nextActivityIntent = new Intent();

        // Determine which activity to go to based on position of list item clicked:
        switch (position)
        {
            case PICTURE_ACTIVITY_POSITION:
                nextActivityIntent.setClass(this, CalendarActivity.class);
                break;
            case COUNTER_ACTIVITY_POSITION:
                nextActivityIntent.setClass(this, CounterActivity.class);
                break;
            case ACTIVITY_2:
                nextActivityIntent.setClass(this, WebActivity.class);
                break;
            case ACTIVITY_3:
                nextActivityIntent.setClass(this, NinjaActivity.class);
                break;
            default:
                Toast oopsToast = Toast.makeText(v.getContext(), "Sorry, that option didn't work.", Toast.LENGTH_SHORT);
                oopsToast.show();
                break;
        }

        startActivity(nextActivityIntent);
        overridePendingTransition(R.anim.scale_from_top_left, R.anim.abc_shrink_fade_out_from_bottom);

    }
}
