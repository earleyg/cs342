package edu.carleton.earleyg.funlist;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import biweekly.Biweekly;
import biweekly.ICalendar;
import biweekly.component.VEvent;
import biweekly.property.DateStart;
import biweekly.util.DateTimeComponents;
import biweekly.util.ICalDate;


public class CalendarActivity extends Activity {

    // Taken from Carleton's convocation Calendar file (.ics):
    private final String CONVO_CALENDAR_ICS_STRING = "BEGIN:VCALENDAR\n" +
            "VERSION:2.0\n" +
            "PRODID:-//Reason 4.0//EN\n" +
            "X-WR-CALNAME;VALUE=TEXT:Convocations\n" +
            "X-WR-TIMEZONE:America/Chicago\n" +
            "BEGIN:VEVENT\n" +
            "UID:20140925145221-1188175@carleton.edu\n" +
            "SUMMARY:Convocation: Becky Morrison\n" +
            "DESCRIPTION:Founder of Globetops, Becky Morrison collects and refurbishes electronic wa\n" +
            " ste, converting it into a usable instrument of world change.\n" +
            "LOCATION:Skinner Chapel\n" +
            "LAST-MODIFIED:20150319T085520\n" +
            "CREATED:20140925T145221\n" +
            "DTSTART:20150410T105000\n" +
            "DURATION:PT1H0M0S\n" +
            "END:VEVENT\n" +
            "BEGIN:VEVENT\n" +
            "UID:20140925145436-1188177@carleton.edu\n" +
            "SUMMARY:Convocation: Arsalan Iftikhar\n" +
            "DESCRIPTION:International human rights lawyer Arsalan Iftikhar is a global media commen\n" +
            " tator frequently sought for his American-Muslim perspective.\n" +
            "LOCATION:Skinner Chapel\n" +
            "LAST-MODIFIED:20150303T104802\n" +
            "CREATED:20140925T145436\n" +
            "DTSTART:20150417T105000\n" +
            "DURATION:PT1H0M0S\n" +
            "END:VEVENT\n" +
            "BEGIN:VEVENT\n" +
            "UID:20140925145818-1188180@carleton.edu\n" +
            "SUMMARY:Convocation: Daniel Kurtzer\n" +
            "DESCRIPTION:Foreign service veteran Daniel Kurtzer examines U.S. policy toward the Midd\n" +
            " le East peace process.\n" +
            "LOCATION:Skinner Chapel\n" +
            "LAST-MODIFIED:20150304T163132\n" +
            "CREATED:20140925T145818\n" +
            "DTSTART:20150424T105000\n" +
            "DURATION:PT1H0M0S\n" +
            "END:VEVENT\n" +
            "BEGIN:VEVENT\n" +
            "UID:20140925150852-1188187@carleton.edu\n" +
            "SUMMARY:Convocation: Denise Dunning\n" +
            "DESCRIPTION:Founder of Let Girls Lead, Denise Dunning seeks to ensure that girls can at\n" +
            " tend school, stay healthy, and learn skills to escape poverty.\n" +
            "LOCATION:Skinner Chapel\n" +
            "LAST-MODIFIED:20140925T151209\n" +
            "CREATED:20140925T150852\n" +
            "DTSTART:20150501T105000\n" +
            "DURATION:PT1H0M0S\n" +
            "END:VEVENT\n" +
            "BEGIN:VEVENT\n" +
            "UID:20140925151216-1188196@carleton.edu\n" +
            "SUMMARY:Convocation: Ragamala Dance\n" +
            "DESCRIPTION:Ragamala Dance carries South Indian classical dance into the 21st century.\n" +
            "LOCATION:Skinner Chapel\n" +
            "LAST-MODIFIED:20140925T151404\n" +
            "CREATED:20140925T151216\n" +
            "DTSTART:20150508T105000\n" +
            "DURATION:PT1H0M0S\n" +
            "END:VEVENT\n" +
            "BEGIN:VEVENT\n" +
            "UID:20140925151409-1188203@carleton.edu\n" +
            "SUMMARY:Convocation: Chude Allen ’65\n" +
            "DESCRIPTION:A Freedom Summer participant, Chude Allen relates the experience of African\n" +
            " -American voter registration in Mississippi in 1964.\n" +
            "LOCATION:Skinner Chapel\n" +
            "LAST-MODIFIED:20150320T162810\n" +
            "CREATED:20140925T151409\n" +
            "DTSTART:20150515T105000\n" +
            "DURATION:PT1H0M0S\n" +
            "END:VEVENT\n" +
            "BEGIN:VEVENT\n" +
            "UID:20140925151900-1188213@carleton.edu\n" +
            "SUMMARY:Convocation: Esera Tuaolo\n" +
            "DESCRIPTION:Former professional football player Esera Tuaolo relates the story of his l\n" +
            " ife as a gay man in the NFL.\n" +
            "LOCATION:Skinner Chapel\n" +
            "LAST-MODIFIED:20150303T105646\n" +
            "CREATED:20140925T151900\n" +
            "DTSTART:20150522T105000\n" +
            "DURATION:PT1H0M0S\n" +
            "END:VEVENT\n" +
            "BEGIN:VEVENT\n" +
            "UID:20140925152108-1188219@carleton.edu\n" +
            "SUMMARY:Honors Convocation\n" +
            "DESCRIPTION:Annual ceremony that draws the campus community together to celebrate the a\n" +
            " wards and academic accomplishments of our students.\n" +
            "LOCATION:Skinner Chapel\n" +
            "LAST-MODIFIED:20150316T102205\n" +
            "CREATED:20140925T152108\n" +
            "DTSTART:20150529T150000\n" +
            "DURATION:PT1H0M0S\n" +
            "END:VEVENT\n" +
            "END:VCALENDAR\n";

    GregorianCalendar calendar;

    Date today;
    ICalendar convos;
    VEvent nextConvo;

    TextView convoDescriptionText;
    TextView convoDateText;
    TextView convoSpeakerNameText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        calendar = new GregorianCalendar();

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        today = calendar.getTime();

        convos = Biweekly.parse(CONVO_CALENDAR_ICS_STRING).first();
        nextConvo = getNextConvo();

        convoDescriptionText = (TextView) findViewById(R.id.convoDescripText);
        convoDateText = (TextView) findViewById(R.id.dateText);
        convoSpeakerNameText = (TextView) findViewById(R.id.speakerNameText);

        updateEventInfoDisplay();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calendar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.scale_from_top_left, R.anim.abc_shrink_fade_out_from_bottom);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private VEvent getNextConvo() {
        int shortestDaysBetween = Integer.MAX_VALUE;
        VEvent nextConvo = null;

        for (VEvent convoEvent : convos.getEvents()) {

            int daysBetween = daysFromToday(convoEvent);

            if (daysBetween < shortestDaysBetween) {
                shortestDaysBetween = daysBetween;
                nextConvo = convoEvent;
            }
        }

        return nextConvo;
    }

    private int daysFromToday(VEvent event) {
        DateStart dtstart = event.getDateStart();
        ICalDate date = dtstart.getValue();

        int month = date.getRawComponents().getMonth();
        int day = date.getRawComponents().getDate();
        int year = date.getRawComponents().getYear();

        GregorianCalendar eventCal = new GregorianCalendar(year, month, day);
        Date eventDate = eventCal.getTime();

        int daysBetween = Days.daysBetween(new LocalDate(today), new LocalDate(eventDate)).getDays();

        return daysBetween;
    }

    private void updateEventInfoDisplay() {
        if (nextConvo != null) {
            DateTimeComponents components = nextConvo.getDateStart().getValue().getRawComponents();
            Integer month = components.getMonth();
            Integer day = components.getDate();
            Integer year = components.getYear();

            String convoDateString = String.format("%s/%s/%s", month, day, year);

            convoDateText.setText(convoDateString);
            convoSpeakerNameText.setText(nextConvo.getSummary().getValue());
            convoDescriptionText.setText(nextConvo.getDescription().getValue());
        }

        else {
            convoSpeakerNameText.setText(getString(R.string.no_convo_alert));
        }
    }

}
