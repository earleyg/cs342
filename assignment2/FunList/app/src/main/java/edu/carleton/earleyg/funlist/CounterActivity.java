package edu.carleton.earleyg.funlist;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class CounterActivity extends Activity implements View.OnClickListener {

    MediaPlayer umpireSoundPlayer;

    TextView strikeCountText;
    TextView ballCountText;
    TextView strikeoutCountText;
    TextView walkCountText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counter);

        Button strikeButton = (Button) this.findViewById(R.id.strikeButton);
        strikeButton.setOnClickListener(this);

        Button ballButton = (Button) this.findViewById(R.id.ballButton);
        ballButton.setOnClickListener(this);

        // Counts start at zero:
        strikeCountText = (TextView) findViewById(R.id.strikeCountText);
        strikeCountText.setText("0");

        ballCountText = (TextView) findViewById(R.id.ballCountText);
        ballCountText.setText("0");

        strikeoutCountText = (TextView) findViewById(R.id.strikeoutCount);
        strikeoutCountText.setText("0");

        walkCountText = (TextView) findViewById(R.id.walkCount);
        walkCountText.setText("0");

        umpireSoundPlayer = MediaPlayer.create(this, R.raw.strike3);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_counter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.scale_from_top_left, R.anim.abc_shrink_fade_out_from_bottom);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {

        Integer count;

        switch(view.getId()) {
            case R.id.strikeButton:
                count = Integer.parseInt((String) strikeCountText.getText());
                count++;
                strikeCountText.setText(count.toString());
                updateStrikouts(count);
                break;

            case R.id.ballButton:
                count = Integer.parseInt((String) ballCountText.getText());
                count++;
                ballCountText.setText(count.toString());
                updateWalks(count);
                break;

            default:
                break;
        }
    }


    private void updateStrikouts(Integer currentStrikeCount) {
        TextView strikeoutCountText = (TextView) findViewById(R.id.strikeoutCount);
        Integer strikeoutCount = Integer.parseInt((String) strikeoutCountText.getText());

        if (currentStrikeCount == 3) {
            strikeoutCount++;
            strikeoutCountText.setText(strikeoutCount.toString());
            resetCount(); // Clear the count!

            if (umpireSoundPlayer.isPlaying()){
                umpireSoundPlayer.stop();
                try {
                    umpireSoundPlayer.prepare();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            umpireSoundPlayer.start();

        }
    }

    public void updateWalks(Integer currentBallCount) {
        Integer walkCount = Integer.parseInt((String) walkCountText.getText());

        if (currentBallCount == 4) {
            walkCount++;
            walkCountText.setText(walkCount.toString());
            resetCount(); // Clear the count!
        }
    }

    private void resetCount() {
        ballCountText.setText("0");
        strikeCountText.setText("0");
    }
}
